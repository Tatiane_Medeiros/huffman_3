#ifndef ZIP
#define ZIP
#include <QFile>
#include <QTextStream>
#include <QBitArray>
#include "HTree.h"



//frequência de cada caractere no arquivo
int* countbytes(QString name);
//exbe o array das frequências
void show(int *array);
//codifica o arquivo usando a arvore de huffman.
QByteArray Coding(QString name);
//transforma a codificação binaria em bytes e retorna o lixo
QPair<QByteArray, int> toByte(QByteArray bits);




#endif // ZIP

