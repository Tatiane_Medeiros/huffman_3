#-------------------------------------------------
#
# Project created by QtCreator 2015-05-12T15:00:35
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Huffman_3
CONFIG   += console
CONFIG   -= app_bundle


TEMPLATE = app


SOURCES += main.cpp \
    HTree.cpp \
    zip.cpp

HEADERS += \
    Node.h \
    HTree.h \
    zip.h
