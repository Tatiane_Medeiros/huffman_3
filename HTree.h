#ifndef HTREE_H
#define HTREE_H
#include "Node.h"
#include <QList>
#include <QByteArray>
#include <iostream>
#include <QPair>
#include <QHash>


class HTree
{
private:
    Node* m_root;
    //ordena a lista
    QList<Node*> insort(const QList<Node*> mylist);

public:
    HTree();
    HTree(Node* root);
    ~HTree();
    //constói a arvore de Huffman
    void buildTree(QList<Node *> myList);
    //
    QList<Node*> toList(int *array);
    //cria a representação da arvore
    QByteArray representation();
    //retorna a codificação de um caractere na arvore
    QByteArray code(const uchar cont);
    //lista as codificações de todos os caracteres do arquivo
    QHash<uchar, QByteArray> codeRef(QList<Node *> myList);
    //exibe a arvore
    void showTree();



};


#endif // HTREE_H
