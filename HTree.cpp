#include "HTree.h"

HTree::HTree()
{
    m_root = new Node();
}

HTree::HTree(Node* root)
{
    m_root = root;
}

HTree::~HTree()
{
    delete this;
}

QList<Node *> HTree::toList(int *array)
{
    QList<Node* > list;

    for(int i = 0; i<256; ++i){
        if(array[i] != 0){
            Node* tmp = new Node(i, array[i], NULL, NULL);
            list.append(tmp);
        }
    }

    return list;
}


void HTree::buildTree(QList<Node *> myList){
    QList<Node *> nodeList = myList;

    while(nodeList.size() > 1){
        nodeList = insort(nodeList);
        Node* left = nodeList.at(0); Node* right = nodeList.at(1);
        Node* aux = new Node(0x2A, left->m_freq + right->m_freq, left, right);
        nodeList.removeFirst();
        nodeList.removeFirst();
        nodeList.prepend(aux);

    }

    m_root = nodeList.at(0);

}


QByteArray HTree::representation(){
    QByteArray aux;
    if(m_root == 0) return "";

       if(m_root->isLeaf()){
           if(m_root->m_content == 0x28 || m_root->m_content == 0x2A) aux.append(0x2A);
           aux.append(m_root->m_content);
       }
        else{

            HTree* sub1 = new HTree(m_root->m_left);
            HTree* sub2 = new HTree(m_root->m_right);

            aux.append('(').append(sub1->representation()).append(sub2->representation());

        }

    return aux;

}


QList<Node*> HTree::insort(const QList<Node*> mylist){
        QList<Node*> list = mylist;

      for(int i = 0; i< list.size() - 1; ++i){
         for(int j=i+1; j< list.size(); ++j){
             if(list.at(j)->m_freq < list.at(i)->m_freq){
                 list.swap(i, j);
             }
           }
       }
      return list;
}


QByteArray HTree::code(const uchar cont){
    QByteArray Code;
    if(m_root->isLeaf()){
        if(m_root->m_content == cont && m_root->m_freq != 0) return "";
    }
    else{
        HTree *sub1 = new HTree(m_root->m_right);
        HTree *sub2 = new HTree(m_root->m_left);

        if(!sub1->code(cont).isNull()){
            Code.append("1").append(sub1->code(cont));
        }
        else if(!sub2->code(cont).isNull()){
            Code.append("0").append(sub2->code(cont));
        }
    }

    return Code;

}

QHash<uchar, QByteArray> HTree::codeRef(QList<Node *> myList){
    QByteArray aux;
    uchar c;
    QHash<uchar, QByteArray> ref;
    for(int i = 0; i<myList.size(); ++i){
            c = myList.at(i)->m_content;
            aux = this->code(c);
            ref.insert(c, aux);
           qDebug()<< char(c)<< " codigo:" <<qPrintable(aux);

    }

    return ref;

}

void HTree::showTree(){
    qDebug("arvore:\n%s", qPrintable(m_root->toLine(0,m_root)));
}


