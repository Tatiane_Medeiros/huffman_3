#ifndef NODE
#define NODE
#include <QDebug>
#include <QObject>

class Node{

public:
    uchar m_content;
    int m_freq;
    Node* m_left;
    Node* m_right;

    Node(){
       m_freq = 0;
       m_left = NULL;
       m_right = NULL;
    }

    Node(uchar content, int freq, Node* left, Node* right){
        m_content = content;
        m_freq = freq;
        m_left = left;
        m_right = right;
    }


    //Retorna se o nó é folha
    bool isLeaf(){
        if(m_left == NULL && m_right == NULL) return true;

        return false;
    }


    //Exibe a arvore
    QString toLine(int num, Node* n){
        QString aux;

        aux += QString("   ").repeated(num);

        aux.append(n->m_content).append('\n');

        if(n->isLeaf()) return aux;

        else{
        aux.insert(0,toLine(num + 1, n->m_right));
         aux.append(toLine(num + 1, n->m_left));


        return aux;
        }
    }



};

#endif // NODE

