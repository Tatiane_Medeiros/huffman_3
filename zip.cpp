#include "zip.h"



int* countbytes( QString name){
    int* array = new int[256];

    for(int i = 0; i<256; ++i) array[i] = 0;

    QFile file(name);

    Q_ASSERT_X(file.open(QIODevice::ReadOnly), "Zip::countbytes", "file not found.");

    while (!file.atEnd()) {
         QByteArray line = file.read(1024);
              for(int i = 0; i < line.size(); ++i) {
                 ++array[(unsigned char) line.at(i)];
              }
     }
    file.close();
    return array;

}


void show(int *array){
    for(int i=0; i<256; ++i){
        bool ok;
        uchar a = QByteArray::number(i).toInt(&ok,16);

        if(array[i] != 0) std::cout <<i<<' '<< a<<' '<< array[i]<< std::endl;

    }
   std::cout <<"\n\n";

}


QByteArray Coding(QString name){

    int* freq = countbytes(name);
    HTree* tree = new HTree();
    QList<Node*> mylist = tree->toList(freq);
    tree->buildTree(mylist);
    QHash<uchar, QByteArray> ref = tree->codeRef(mylist);

    QFile myfile(name);
    myfile.open(QIODevice::ReadOnly);
    QByteArray bitArray;

    while (!myfile.atEnd()) {
         QByteArray line = myfile.read(1024);
         for(int i=0; i<line.size(); ++i){
             bitArray.append(ref.value(line.at(i)));
         }
    }
    myfile.close();
    // qDebug()<<"codificacao binaria:" << qPrintable(bitArray+"\n");

    QByteArray code = toByte(bitArray).first;

    return code;


}



QPair<QByteArray, int> toByte(QByteArray bits){
    int len = bits.size();
    int trash = 0;
    if( len%8 != 0){
        trash = 8 - len%8;
        bits.append(QByteArray("0").repeated(trash));
    }


    QByteArray bytes;
    QByteArray aux;
    for(int i=0; i<len; i+=8){
        for(int j=i; j<i+8; ++j){
           aux += bits.at(j);
        }
        bool ok;

        bytes.append(uchar(aux.toInt(&ok, 2)));
        aux.clear();
    }

    return QPair<QByteArray, int> (bytes, trash);

}




